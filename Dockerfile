FROM node:15
WORKDIR /app
COPY vuejs/package.json ./
RUN npm cache clean --force && npm install -g npm@7.4.2 && npm install 
COPY vuejs/* ./
RUN npm run build
